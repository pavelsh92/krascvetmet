<?php $scripts[] = ''; ?>

<!-- ======== BOOTSTRAP ======== -->
<script src="vendor/bootstrap/transition.js"></script>
<script src="vendor/bootstrap/carousel.js"></script>
<script src="vendor/bootstrap/modal.js"></script>
<script src="vendor/bootstrap/dropdown.js"></script>
<script src="vendor/bootstrap/alert.js"></script>
<script src="vendor/bootstrap/tooltip.js"></script>
<script src="vendor/bootstrap/popover.js"></script>
<script src="vendor/bootstrap/tab.js"></script>
<script src="vendor/bootstrap/collapse.js"></script>
<script src="vendor/bootstrap/affix.js"></script>
<script src="vendor/bootstrap/scrollspy.js"></script>

<!-- ======== 3RD PARTY LIBRARIES ======== -->

<script src="vendor/chosen.jquery.min.js"></script>
<script src="vendor/jquery.maskedinput.min.js"></script>
<script src="vendor/dropzone.js"></script>
<script src="vendor/moment.js"></script>
<script src="vendor/jquery.daterangepicker.min.js"></script>

<!-- ======== CUSTOM ======== -->

<script src="js/globals.js"></script>
<script src="js/onDocReady.js"></script>
<script src="js/onWindowLoad.js"></script>

<?  $scripts = array_unique($scripts); // removing duplicates
    foreach ($scripts as $script): ?>
    <script src="<?= $script?>"></script>
<?  endforeach; ?>