<div class="modal fade login" tabindex="-1" role="dialog" id="login">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active error" id="sign_in">
          <div class="modal-body">
            <h4 class="modal-title">
              ВХОД
              <a class="pseudolink reg" href="#sign_up" aria-controls="sign_up" role="tab" data-toggle="tab">
                <span class="text">РЕГИСТРАЦИЯ</span>
              </a>
            </h4>
            <form>
              <label>
                <span class="name">Эл. почта</span>
                <input type="text">
              </label>
              <label>
                <span class="name">Пароль</span>
                <span class="pass-wrap">
                  <input type="password">
                  <div class="btn-pwdswitch"></div>
                </span>
              </label>
              <button action="" type="button" class="btn btn-primary">Войти</button>
              <a href="#new_pass" type="button" class="pseudolink" aria-controls="new_pass" role="tab" data-toggle="tab">Забыли пароль?</a>
            </form>
            <div class="message-error">
              Неправильно указан логин или пароль
            </div>
          </div>
        </div>


        <div role="tabpanel" class="tab-pane" id="sign_up">
          <div class="modal-body">
            <h4 class="modal-title">
              <a class="pseudolink" href="#sign_in" aria-controls="sign_in" role="tab" data-toggle="tab">
                <span class="text">ВХОД</span>
              </a>
              <span class="reg">РЕГИСТРАЦИЯ</span>
            </h4>
            <form>
              <label class="radio-styled radio-inline">
                <input type="radio" name="radio-reg" value="1" checked> <span class="text">Юридическое лицо</span>
              </label>
              <label class="radio-styled radio-inline">
                <input type="radio" name="radio-reg" value="2"> <span class="text">Физическое лицо</span>
              </label>

              <label class="input-text js-org-hid">
                <span class="name">Название организации</span>
                <input type="text">
              </label>
              <label class="input-text">
                <span class="name">Эл. почта</span>
                <input type="text">
              </label>
              <label class="input-text">
                <span class="name">Пароль</span>
                <input type="text">
              </label>
              <button type="button" class="btn btn-primary">Зарегистрироваться</button>
              <div class="agreement">
                Регистрируясь я соглашаюсь с <a href="" class="link"><span class="text">политикой обработки персональных данных</span></a>
              </div>
            </form>
          </div>
        </div>

        <div role="tabpanel" class="tab-pane" id="new_pass"> <!-- toggled class .new_pass-success for success form -->
          <div class="modal-body">
            <h4 class="modal-title">
              Выслать новый пароль
            </h4>
            <form>
              <label class="input-text">
                <span class="name">Эл. почта</span>
                <input type="text">
              </label>
              <div class="back">
                <a href="#sign_in" type="button" class="pseudolink" aria-controls="sign_in" role="tab" data-toggle="tab">Назад</a>
              </div><!-- not delete
            --><button type="button" class="btn btn-primary">Выслать новый пароль</button>
          </form>
          <div class="success">
            <div class="message">Новый пароль выслан на указанную почту.</div>
            <a href="#sign_in" type="button" aria-controls="sign_in" role="tab" data-toggle="tab" class="btn btn-primary">Вернуться на вход</a>
          </div>
        </div>
      </div>
    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade modal-text" tabindex="-1" role="dialog" id="modal-order">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

      <h4 class="modal-title">
        Заявка подана
      </h4>
      <div class="modal-body">
        <p>Спасибо за заявку! Вам перезвонит наш менеджер в течение 15 мин.</p>
        <p>Следить за статусом заявки вы можете в разделе.</p>
        <div>
                  <button type="button" class="btn btn-primary">Мои заявки</button>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade modal-text" tabindex="-1" role="dialog" id="modal-saved">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

      <h4 class="modal-title">
        Данные сохранены
      </h4>
      <div class="modal-body">
        Вы можете закрыть браузер и вернуться к заполнению в любое время.
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade modal-text" tabindex="-1" role="dialog" id="modal-auth-or-not">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

      <h4 class="modal-title">
        Вход в личный кабинет
      </h4>
      <div class="modal-body">
        Если вы уже присоединились к нам, Авторизируйтесь и часть полей заполнится автоматически

        <div>
          <button type="button" class="btn btn-primary">Авторизироваться</button>
          <button type="button" class="btn btn-default">Продолжить без авторизации</button>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->