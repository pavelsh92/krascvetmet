<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link rel="stylesheet" href="css/s.min.css">
  <link rel="stylesheet" href="css/daterangepicker.min.css">
  <script type="text/javascript" src="vendor/jquery-3.2.1.min.js"></script>
  <!-- <script type="text/javascript" src="vendor/jquery-1.8.3.min.js"></script> -->

  <script src="svg/all.js"></script>
</head>
<body>

  <!-- SVG sprite include -->
  <div class="svg-placeholder"
    style="border: 0; clip: rect(0 0 0 0); height: 1px;
      margin: -1px; overflow: hidden; padding: 0;
      position: absolute; width: 1px;"></div>
  <script>
    document.querySelector('.svg-placeholder').innerHTML = SVG_SPRITE;
  </script> 
  <!-- end SVG sprite include -->

  <div class="main-wrapper">