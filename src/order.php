<? include('modules/header.php'); ?>
<div class="head-block">
  <div class="head-block__menu">
    <div class="container">
      <nav>
        <ul>
          <li><a href="">Расчёт стоимости присоединения</a></li>
          <li class="active"><a href="">Мои заявки</a></li>
          <li><a href="">Сообщения<span class="quan">15</span></a></li>
          <li><a href="">Профиль</a></li>
        </ul>
      </nav>
    </div>
  </div>
  <div class="head-block__title">
    <div class="container">
      <div class="title">мои заявки</div>
      <a href="calc.php" class="btn btn-primary">Добавить новую заявку</a>
    </div>
  </div>

</div>
<div class="container order">
  <div class="row">
    <div class="order__filter col-lg-3">
      <form>
        <div class="form__elem">
          <label>
            <div class="input-name">Номер заявки</div>
            <input type="text" class="number">
          </label>
        </div>
        <div class="form__elem">
          <div class="input-name">Статус заявки</div>
          <select name="" id="" class="form-control chosen-select">
            <option value="1">Lorem</option>
            <option value="2">ipsum</option>
            <option value="3">dolor</option>
            <option value="4">sit</option>
            <option value="5">amet,</option>
            <option value="6">consectetur</option>
          </select>
        </div>
        <div class="form__elem">
          <div class="input-name">Дата подачи заявки</div>
          <div class="date">
            <div class="dateRangePicker"><input type="text"></div>
          </div>
        </div>
        <div class="form__elem">
          <label class="check-styled">
            <input type="checkbox" name="check17641235" value="1">
            <span class="text">
              С прикреплёнными при заявке документами
            </span>
          </label>
        </div>
      </form>
    </div>
    <div class="order__list col-lg-9">
      <div class="table">
        <div class="table__row table__row-head">
          <div class="number">№</div>
          <div class="object">Объект подключения</div>
          <div class="status">Статус</div>
          <div class="date1">Дата подачи</div>
          <div class="date2">Дата изменения</div>
          <div class="power">Мощность, кВт</div>
          <div class="class">Класс напряжения, кВ</div>
        </div>


        <div class="table__row">
          <div class="number">178542</div>
          <div class="object">
            <div class="name">Строительный объект оп адресу ул. Ленина 128 строение большое и красивое, словно 8 чудо-света!!!</div>
            <a href="calc.php" class="btn-edit link btn-reset">
              <span class="svg-wrap"><svg><use xlink:href="#pencil"></use></svg></span>
            </a>
          </div>
          <div class="status">Дозапрос документов</div>
          <div class="date1">28 сент. 2017</div>
          <div class="date2">30 сент. 2017</div>
          <div class="power">2 000</div>
          <div class="class">1 000</div>
          <div class="docs">
            <button href="" class="pseudolink-more js-btn-order-docs">Показать документы</button>
            <div class="collapse">
              <div class="docs__inner">
                <div class="docs__title">Документы от заявителя</div>
                <div class="docs__item">
                  <div class="name">Расположение энергоприн...го устройства</div>
                  <div class="details-sample">Принято 14.03.1999</div>
                </div>
                <div class="docs__item">
                  <div class="name">Перечень и мощность энергоприн...их устройств</div>
                  <div class="details-sample">Принято 14.03.1999</div>
                </div>
                <div class="docs__item">
                  <div class="name">Подтверждение полномочий</div>
                  <div class="details-sample">Принято 14.03.1999</div>
                </div>
                <div class="docs__item">
                  <div class="name">Документ на право собственности</div>
                  <div class="details-sample">Принято 14.03.1999</div>
                </div>
              </div>
              <div class="docs__title">Документы от исполнителя</div>
              <div class="docs__item">
                <div class="name">Перечень и мощность энергоприн...их устройств</div>
                <div class="details">
                  <div class="btn-popover-wrap">
                    <button type="button" class="btn-reset" data-placement="bottom" data-toggle="popover" data-content="Подписание 02 окт. 2017 — текущая стадия<br>Согласование 12 сент. 2017">Согласование</button>
                  </div>
                </div>
              </div>
              <div class="docs__item">
                <div class="name">Подтверждение полномочий</div>
                <div class="details">
                  <div class="btn-popover-wrap">
                    <button type="button" class="btn-reset" data-placement="bottom" data-toggle="popover" data-content="Подписание 02 окт. 2017 — текущая стадия<br>Согласование 12 сент. 2017">Согласование</button>
                  </div>
                  <div class="btn-popover-wrap">
                    <button type="button" class="btn-reset" data-placement="bottom" data-toggle="popover" data-content="Подписание 02 окт. 2017 — текущая стадия<br>Согласование 12 сент. 2017">Разработка заявителем проектной документ ... его земельного участка​</button>
                  </div>
                  <div class="btn-popover-wrap">
                    <button type="button" class="btn-reset" data-placement="bottom" data-toggle="popover" data-content="Подписание 02 окт. 2017 — текущая стадия<br>Согласование 12 сент. 2017">Выполнение обязательств</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="table__row">
        <div class="number">178542</div>
        <div class="object">
          <div class="name">Строительный объект оп адресу ул. Ленина 128 строение большое и красивое, словно 8 чудо-света!!!</div>
          <a href="calc.php" class="btn-edit link btn-reset">
            <span class="svg-wrap"><svg><use xlink:href="#pencil"></use></svg></span>
          </a>
        </div>
        <div class="status">Дозапрос документов</div>
        <div class="date1">28 сент. 2017</div>
        <div class="date2">30 сент. 2017</div>
        <div class="power">2 000</div>
        <div class="class">1 000</div>
        <div class="docs">
          <button href="" class="pseudolink-more js-btn-order-docs">Показать документы</button>
          <div class="collapse">
            <div class="docs__inner">
              <div class="docs__title">Документы от заявителя</div>
              <div class="docs__item">
                <div class="name">Расположение энергоприн...го устройства</div>
                <div class="details-sample">Принято 14.03.1999</div>
              </div>
              <div class="docs__item">
                <div class="name">Перечень и мощность энергоприн...их устройств</div>
                <div class="details-sample">Принято 14.03.1999</div>
              </div>
              <div class="docs__item">
                <div class="name">Подтверждение полномочий</div>
                <div class="details-sample">Принято 14.03.1999</div>
              </div>
              <div class="docs__item">
                <div class="name">Документ на право собственности</div>
                <div class="details-sample">Принято 14.03.1999</div>
              </div>
              <div class="docs__title">Документы от исполнителя</div>
              <div class="docs__item">
                <div class="name">Перечень и мощность энергоприн...их устройств</div>
                <div class="details">
                  <div class="btn-popover-wrap">
                    <button type="button" class="btn-reset" data-placement="bottom" data-toggle="popover" data-content="Подписание 02 окт. 2017 — текущая стадия<br>Согласование 12 сент. 2017">Согласование</button>
                  </div>
                </div>
              </div>
              <div class="docs__item">
                <div class="name">Подтверждение полномочий</div>
                <div class="details">
                  <div class="btn-popover-wrap">
                    <button type="button" class="btn-reset" data-placement="bottom" data-toggle="popover" data-content="Подписание 02 окт. 2017 — текущая стадия<br>Согласование 12 сент. 2017">Согласование</button>
                  </div>
                  <div class="btn-popover-wrap">
                    <button type="button" class="btn-reset" data-placement="bottom" data-toggle="popover" data-content="Подписание 02 окт. 2017 — текущая стадия<br>Согласование 12 сент. 2017">Разработка заявителем проектной документ ... его земельного участка​</button>
                  </div>
                  <div class="btn-popover-wrap">
                    <button type="button" class="btn-reset" data-placement="bottom" data-toggle="popover" data-content="Подписание 02 окт. 2017 — текущая стадия<br>Согласование 12 сент. 2017">Выполнение обязательств</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="table__row">
        <div class="number">178542</div>
        <div class="object">
          <div class="name">Строительный объект оп адресу ул. Ленина 128 строение большое и красивое, словно 8 чудо-света!!!</div>
          <span class="btn-edit disabled link btn-reset">
            <span class="svg-wrap"><svg><use xlink:href="#pencil"></use></svg></span>
          </span>
        </div>
        <div class="status">Дозапрос документов</div>
        <div class="date1">28 сент. 2017</div>
        <div class="date2">30 сент. 2017</div>
        <div class="power">2 000</div>
        <div class="class">1 000</div>
        <div class="docs">
          <button href="" class="pseudolink-more js-btn-order-docs">Показать документы</button>
          <div class="collapse">
            <div class="docs__inner">
              <div class="docs__title">Документы от заявителя</div>
              <div class="docs__item">
                <div class="name">Расположение энергоприн...го устройства</div>
                <div class="details-sample">Принято 14.03.1999</div>
              </div>
              <div class="docs__item">
                <div class="name">Перечень и мощность энергоприн...их устройств</div>
                <div class="details-sample">Принято 14.03.1999</div>
              </div>
              <div class="docs__item">
                <div class="name">Подтверждение полномочий</div>
                <div class="details-sample">Принято 14.03.1999</div>
              </div>
              <div class="docs__item">
                <div class="name">Документ на право собственности</div>
                <div class="details-sample">Принято 14.03.1999</div>
              </div>
            </div>
            <div class="docs__title">Документы от исполнителя</div>
            <div class="docs__item">
              <div class="name">Перечень и мощность энергоприн...их устройств</div>
              <div class="details">
                <div class="btn-popover-wrap">
                  <button type="button" class="btn-reset" data-placement="bottom" data-toggle="popover" data-content="Подписание 02 окт. 2017 — текущая стадия<br>Согласование 12 сент. 2017">Согласование</button>
                </div>
              </div>
            </div>
            <div class="docs__item">
              <div class="name">Подтверждение полномочий</div>
              <div class="details">
                <div class="btn-popover-wrap">
                  <button type="button" class="btn-reset" data-placement="bottom" data-toggle="popover" data-content="Подписание 02 окт. 2017 — текущая стадия<br>Согласование 12 сент. 2017">Согласование</button>
                </div>
                <div class="btn-popover-wrap">
                  <button type="button" class="btn-reset" data-placement="bottom" data-toggle="popover" data-content="Подписание 02 окт. 2017 — текущая стадия<br>Согласование 12 сент. 2017">Разработка заявителем проектной документ ... его земельного участка​</button>
                </div>
                <div class="btn-popover-wrap">
                  <button type="button" class="btn-reset" data-placement="bottom" data-toggle="popover" data-content="Подписание 02 окт. 2017 — текущая стадия<br>Согласование 12 сент. 2017">Выполнение обязательств</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>

<? include('modules/footer.php'); ?>